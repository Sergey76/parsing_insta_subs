import json

import requests
import time
import random
HEADERS = {'Host': 'www.instagram.com',
           'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64; rv:80.0) Gecko/20100101 Firefox/80.0',
           'Accept': '*/*',
           'Accept-Language': 'ru-RU,ru;q=0.8,en-US;q=0.5,en;q=0.3',
           'Accept-Encoding': 'gzip, deflate, br',
           'X-CSRFToken': 'k7FUwoyXSQsHzxLkDCU1XvotJiA1WCFW',
           'X-IG-App-ID': '936619743392459',
           'X-IG-WWW-Claim': 'hmac.AR0zB_QIXwqkKjopzSG6Rj3P59nIpVpy0W3pD2jaDPkmuqAa',
           'X-Requested-With': 'XMLHttpRequest',
           'Connection': 'keep-alive',
           'Referer': 'https://www.instagram.com/swisllian/followers/',
           'Cookie': 'ig_did=62C43759-5215-4AAC-8D7E-4199FBF7CDDB; mid=X1oRJAALAAHwVgTG0MlPsVVil8QC; csrftoken=k7FUwoyXSQsHzxLkDCU1XvotJiA1WCFW; fbm_124024574287414=base_domain=.instagram.com; ds_user_id=38174524174; sessionid=38174524174%3ABK9C4SEdy7tegR%3A25; shbid=2701; shbts=1601013616.7742999; rur=FTW; urlgen="{\"185.197.35.109\": 206066}:1kLgmx:xsdYQWagDQ9x31cIRKmyuEQdE5A"',
           'TE': 'Trailers'}
base_url='https://www.instagram.com/graphql/query/?'
list_subs = [] 
has_next_page=True
after = None
while has_next_page:
    after_value = f',"after":"{after}"' if after else ''
    get_params ={
        'query_hash':'c76146de99bb02f6415203be841dd25a',
        'variables':f'{{"id":"1527647026","include_reel":true,"fetch_mutual":true,"first":50{after_value}}}'
    }
    response = requests.get(base_url, headers=HEADERS, params=get_params).json()
    after=response['data']['user']['edge_followed_by']['page_info']['end_cursor']
    has_next_page=response['data']['user']['edge_followed_by']['page_info']['has_next_page']
    # print(response['data']['user']['edge_followed_by']['edges'])
    with open("data_file.json", "a") as write_file:
        for data in response['data']['user']['edge_followed_by']['edges']:
            json.dump(data, write_file, indent=4)
    # list_subs.extend(response['data']['user']['edge_followed_by']['edges'])
    print(response)
    time.sleep(10 + 10 * random.random())
